 <?php

require('connect.php');
require('header.php');

?>

<?php
$query=mysqli_query($connect,'SELECT * FROM digi_card WHERE id="'.$_SESSION['card_id_inprocess'].'" ');

if(mysqli_num_rows($query)==0){
	echo '<meta http-equiv="refresh" content="0;URL=index.php">';
}else {
	$row=mysqli_fetch_array($query);
}
$query2=mysqli_query($connect,'SELECT * FROM card_headers_visibility WHERE digi_card_id ="'.$_SESSION['card_id_inprocess'].'" ');

if(mysqli_num_rows($query2)==0){
	$insert_digi1=mysqli_query($connect,'INSERT INTO card_headers_visibility (digi_card_id) VALUES ("'.$row['id'].'")');
}else {
	$visibility=mysqli_fetch_array($query2);
}
$query3=mysqli_query($connect,'SELECT * FROM card_headers WHERE digi_card_id ="'.$_SESSION['card_id_inprocess'].'" ');

if(mysqli_num_rows($query3)==0){
	$insert_digi2=mysqli_query($connect,'INSERT INTO card_headers (digi_card_id) VALUES ("'.$row['id'].'")');
}else {
	$headers=mysqli_fetch_array($query3);
}

?>

<div class="main3">
<div class="navigator_up">
		<a href="select_theme.php"><div class="nav_cont  " ><i class="fa fa-map"></i> Select Theme</div></a>
		<a href="create_card2.php"><div class="nav_cont "><i class="fa fa-bank"></i> Company Details</div></a>
		<a href="create_card3.php"><div class="nav_cont "><i class="fa fa-facebook"></i> Social Links</div></a>
		<a href="create_card4.php"><div class="nav_cont"><i class="fa fa-rupee"></i> Payment Options</div></a>
		<a href="create_card5.php"><div class="nav_cont "><i class="fa fa-ticket"></i> Products & Services</div></a>
		<a href="create_card7.php"><div class="nav_cont"><i class="fa fa-archive"></i> Order Page</div></a>
		<a href="create_card6.php"><div class="nav_cont"><i class="fa fa-image"></i> Image Gallery</div></a>
		<a href="create_card8.php"><div class="nav_cont active"><i class="fa fa-cog"></i> Settings</div></a>
		<a href="preview_page.php"><div class="nav_cont"><i class="fa fa-laptop"></i> Preview Card</div></a>
	
	</div>
	
	<div class="btn_holder">
		<a href="create_card6.php"><div class="back_btn"><i class="fa fa-chevron-circle-left"></i> Back</div></a>
		<a href="preview_page.php"><div class="skip_btn">Skip <i class="fa fa-chevron-circle-right"></i></div></a>
	</div>
	<h1>Settings</h1>
	
	<form action="" method="POST" enctype="multipart/form-data">
	

<!-------------------form ----------------------->	

	<h3>Visibility Settings</h3>
  	<div class="input_box" >
	   <div class="visibility">
	 <input type="checkbox" name="about_us_visibility" value="<?php if($visibility['about_us'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['about_us'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide About Us Section<br>
	 <input type="checkbox" name="our_offers_visibility" value="<?php if($visibility['our_offers'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['our_offers'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide Our Offer Section<br>
	 <input type="checkbox" name="youtube_videos_visibility" value="<?php if($visibility['youtube_videos'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['youtube_videos'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide Youtube Videos Section<br>
	 <input type="checkbox" name="product_services_visibility" value="<?php if($visibility['product_services'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['product_services'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide Products/Services Section<br>
	 <input type="checkbox" name="image_gallery_visibility" value="<?php if($visibility['image_gallery'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['image_gallery'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide Image Gallery Section<br>
	 <input type="checkbox" name="payment_info_visibility" value="<?php if($visibility['payment_info'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['payment_info'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide Payment Info Section<br>
	 <input type="checkbox" name="bank_acc_detail_visibility" value="<?php if($visibility['bank_acc_detail'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['bank_acc_detail'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide Bank Account Detail Section<br>
	 <input type="checkbox" name="gst_number_visibility" value="<?php if($visibility['gst_number'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['gst_number'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide GST Number Section<br>
	 <input type="checkbox" name="feedback_visibility" value="<?php if($visibility['feedback'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['feedback'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide Feedback Section<br>
	 <input type="checkbox" name="contact_us_visibility" value="<?php if($visibility['contact_us'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['contact_us'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide Contact Us Section<br>
      <input type="checkbox" name="unwanted_link_visibility" value="<?php if($visibility['unwanted_link'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['unwanted_link'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide unwanted link <br>
	 <input type="checkbox" name="views_visibility" value="<?php if($visibility['views'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['views'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide view<br>
	 <input type="checkbox" name="appointment_link" value="<?php if($visibility['appointment_link'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['appointment_link'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide Appointment Link<br>
    <!-- QR code line-->
	 <input type="checkbox" name="qr" value="<?php if($visibility['qr'] == 2){echo "2";}else { echo "1";}?>" <?php if($visibility['qr'] == 2){echo "checked";}?>>&nbsp;&nbsp;Hide QR Code<br>
	<!-- QR code line ends-->
</div>
	</div>


		<h3>Customise headers</h3>
		<div class="input_box"><p>About Us Section Heading <b>*&nbsp; </b></p><input type="text" name="d_about_us"  placeholder="Enter About Us Section Heading" value="<?php if(!empty($headers['about_us'])){echo $headers['about_us'];}?>"  required></div>
		<div class="input_box"><p>Nature Of Business (About Us Subheading) <b>*&nbsp; </b></p><input type="text" name="d_nature_business_"  placeholder="Enter Nature Of Business Heading" value="<?php if(!empty($headers['nature_business_abt'])){echo $headers['nature_business_abt'];}?>"  required></div>
		<div class="input_box"><p>Specialities(About Us Subheading) <b>*&nbsp; </b></p><input type="text" name="d_specialities_abt"  placeholder="Enter Specialities Heading" value="<?php if(!empty($headers['specialities_abt'])){echo $headers['specialities_abt'];}?>"  required></div>
		<div class="input_box"><p>Company Name(About Us Subheading) <b>*&nbsp; </b></p><input type="text" name="d_company_name_abt"  placeholder="Enter Company Name Heading" value="<?php if(!empty($headers['company_name_abt'])){echo $headers['company_name_abt'];}?>"  required></div>
		<div class="input_box"><p>Our Offer Section Heading <b>*&nbsp; </b></p><input type="text" name="d_our_offer"  placeholder="Enter Our Offer Section Heading" value="<?php if(!empty($headers['our_offer'])){echo $headers['our_offer'];}?>"  required></div>
		<div class="input_box"><p>Product & Service Section Heading <b>*&nbsp; </b></p><input type="text" name="d_product_services"  placeholder="Enter Product & Service Section Heading" value="<?php if(!empty($headers['product_services'])){echo $headers['product_services'];}?>"  required></div>
		<div class="input_box"><p>Youtube Videos Section Heading <b>*&nbsp; </b></p><input type="text" name="d_youtube_videos"  placeholder="Enter Youtube Videos Section Heading" value="<?php if(!empty($headers['youtube_videos'])){echo $headers['youtube_videos'];}?>"  required></div>
		<div class="input_box"><p>Image Gallery Section Heading <b>*&nbsp; </b></p><input type="text" name="d_gallery"  placeholder="Enter Image Gallery Section Heading" value="<?php if(!empty($headers['gallery'])){echo $headers['gallery'];}?>"  required></div>
		<div class="input_box"><p>Payment Section Heading <b>*&nbsp; </b></p><input type="text" name="d_payment"  placeholder="Enter Payment Section Heading" value="<?php if(!empty($headers['payment'])){echo $headers['payment'];}?>"  required></div>
		<div class="input_box"><p>Bank Account Detail Section Heading <b>*&nbsp; </b></p><input type="text" name="d_bank_detail"  placeholder="Enter Bank Account Detail Section Heading" value="<?php if(!empty($headers['bank_detail'])){echo $headers['bank_detail'];}?>"  required></div>
		<div class="input_box"><p>Contact Us Section Heading <b>*&nbsp; </b></p><input type="text" name="d_contact"  placeholder="Enter Contact Us Section Heading" value="<?php if(!empty($headers['contact'])){echo $headers['contact'];}?>"  required></div>
		<div class="input_box"><p>Feedback Section Heading <b>*&nbsp; </b></p><input type="text" name="d_feedback"  placeholder="Enter Feedback Section Heading" value="<?php if(!empty($headers['feedback'])){echo $headers['feedback'];}?>"  required></div>
		<div class="input_box"><p>GST Section Heading <b>*&nbsp; </b></p><input type="text" name="d_gst"  placeholder="Enter GST Nuber Section Heading" value="<?php if(!empty($headers['gst'])){echo $headers['gst'];}?>"  required></div>
    	<div class="input_box"><p>Paytm(Payment Subheading) <b>*&nbsp; </b></p><input type="text" name="d_paytm_pay"  placeholder="Enter Paytm Heading" value="<?php if(!empty($headers['paytm_pay'])){echo $headers['paytm_pay'];}?>"  required></div>
    	<div class="input_box"><p>Google Pay(Payment Subheading) <b>*&nbsp; </b></p><input type="text" name="d_googlepay_pay"  placeholder="Enter Google Pay Heading" value="<?php if(!empty($headers['googlepay_pay'])){echo $headers['googlepay_pay'];}?>"  required></div>
    	<div class="input_box"><p>PhonePe(Payment Subheading) <b>*&nbsp; </b></p><input type="text" name="d_phonepe_pay"  placeholder="Enter Phone Pe Heading" value="<?php if(!empty($headers['phonepe_pay'])){echo $headers['phonepe_pay'];}?>"  required></div>
    	<div class="input_box"><p>Name(Bank Detail Subheading) <b>*&nbsp; </b></p><input type="text" name="d_name_bank"  placeholder="Enter Name Heading" value="<?php if(!empty($headers['name_bank'])){echo $headers['name_bank'];}?>"  required></div>
    	<div class="input_box"><p>Account Number(Bank Detail Subheading) <b>*&nbsp; </b></p><input type="text" name="d_accno_bank"  placeholder="Enter Account Number Heading" value="<?php if(!empty($headers['accno_bank'])){echo $headers['accno_bank'];}?>"  required></div>
    	<div class="input_box"><p>IFSC Code(Bank Detail Subheading) <b>*&nbsp; </b></p><input type="text" name="d_ifsc_bank"  placeholder="Enter IFSC Code Heading" value="<?php if(!empty($headers['ifsc_bank'])){echo $headers['ifsc_bank'];}?>"  required></div>
    	<div class="input_box"><p>BANK Name(Bank Detail Subheading) <b>*&nbsp; </b></p><input type="text" name="d_bank_name_bank"  placeholder="Enter Bank Name Heading" value="<?php if(!empty($headers['bank_name_bank'])){echo $headers['bank_name_bank'];}?>"  required></div>
    	<div class="input_box"><p>GST No(GST Subheading) <b>*&nbsp; </b></p><input type="text" name="d_gst_no_gst"  placeholder="Enter GST No. Heading" value="<?php if(!empty($headers['gst_no_gst'])){echo $headers['gst_no_gst'];}?>"  required></div>
			
		
		
		<input type="submit" class="" name="process8" value="Complete & Preview" id="block_loader">
	
<!-------------------form ending----------------------->
	</form>
	
	<?php
	if(isset($_POST['process8'])){
		
		$query=mysqli_query($connect,'SELECT * FROM digi_card WHERE id="'.$_SESSION['card_id_inprocess'].'"');
		if(mysqli_num_rows($query)==1){
			
		// enter details in database
				
			$update1=mysqli_query($connect,'UPDATE card_headers SET 
			
			about_us="'.$_POST['d_about_us'].'",
			nature_business_abt="'.$_POST['d_nature_business_'].'",
			specialities_abt="'.$_POST['d_specialities_abt'].'",
			company_name_abt="'.$_POST['d_company_name_abt'].'",
			our_offer="'.$_POST['d_our_offer'].'",
			product_services="'.$_POST['d_product_services'].'",
			youtube_videos="'.$_POST['d_youtube_videos'].'",
			gallery="'.$_POST['d_gallery'].'",
			payment="'.$_POST['d_payment'].'",
			bank_detail="'.$_POST['d_bank_detail'].'",
			contact="'.$_POST['d_contact'].'",
			feedback="'.$_POST['d_feedback'].'",
			gst="'.$_POST['d_gst'].'",
			paytm_pay="'.$_POST['d_paytm_pay'].'",
			googlepay_pay="'.$_POST['d_googlepay_pay'].'",
			phonepe_pay="'.$_POST['d_phonepe_pay'].'",
			name_bank="'.$_POST['d_name_bank'].'",
			accno_bank="'.$_POST['d_accno_bank'].'",
			ifsc_bank="'.$_POST['d_ifsc_bank'].'",
			bank_name_bank="'.$_POST['d_bank_name_bank'].'",
			gst_no_gst="'.$_POST['d_gst_no_gst'].'"
			WHERE digi_card_id="'.$_SESSION['card_id_inprocess'].'"');
			
			
			isset($_POST['about_us_visibility']) ? $about_us=2 : $about_us=1;
			isset($_POST['our_offers_visibility']) ? $our_offers=2 : $our_offers=1;
			isset($_POST['our_offers_visibility']) ? $youtube_videos=2 : $youtube_videos=1;
			isset($_POST['product_services_visibility']) ? $product_services=2 : $product_services=1;
			isset($_POST['image_gallery_visibility']) ? $image_gallery=2 : $image_gallery=1;
			isset($_POST['bank_acc_detail_visibility']) ? $bank_acc_detail=2 : $bank_acc_detail=1;
			isset($_POST['payment_info_visibility']) ? $payment_info=2 : $payment_info=1;
			isset($_POST['gst_number_visibility']) ? $gst_number=2 : $gst_number=1;
			isset($_POST['feedback_visibility']) ? $feedback=2 : $feedback=1;
			isset($_POST['contact_us_visibility']) ? $contact_us=2 : $contact_us=1;
			isset($_POST['unwanted_link_visibility']) ? $unwanted_link=2 : $unwanted_link=1;
			isset($_POST['views_visibility']) ? $views=2 : $views=1;
			isset($_POST['appointment_link']) ? $appointment_link=2 : $appointment_link=1;			isset($_POST['qr']) ? $qr=2 : $qr=1;//QR code
			
			$update2=mysqli_query($connect,'UPDATE card_headers_visibility SET 
			
			about_us="'.$about_us.'",
			our_offers="'.$our_offers.'",
			youtube_videos="'.$youtube_videos.'",
			product_services="'.$product_services.'",
			image_gallery="'.$image_gallery.'",
			payment_info="'.$payment_info.'",
			bank_acc_detail="'.$bank_acc_detail.'",
			gst_number="'.$gst_number.'",
			feedback="'.$feedback.'",
			unwanted_link="'.$unwanted_link.'",
			views="'.$views.'",
			appointment_link="'.$appointment_link.'",						qr="'.$qr.'",
			contact_us="'.$contact_us.'"
			WHERE digi_card_id="'.$_SESSION['card_id_inprocess'].'"');
			
		// enter details in database ending
	
		if($update1){
			echo '<a href="preview_page.php"><div class="alert info">Details Updated Wait...</div></a>';
			echo '<meta http-equiv="refresh" content="0;URL=preview_page.php">';
			echo '<style>  form {display:none;} </style>';
		}else {
			echo '<a href="create_card3.php"><div class="alert danger">Error! Try Again.</div></a>';
		}
			if($update2){
			echo '<a href="preview_page.php"><div class="alert info">Details Updated Wait...</div></a>';
			echo '<meta http-equiv="refresh" content="0;URL=preview_page.php">';
			echo '<style>  form {display:none;} </style>';
		}else {
			echo '<a href="create_card3.php"><div class="alert danger">Error! Try Again.</div></a>';
		}
			
		
		}else {
			
			echo '<a href="create_card.php"><div class="alert danger">Detail Not Available. Try Again Click here.</div></a>';
		}
		
	}
	?>

</div>


<footer class="">

<p> <?php echo $_SERVER['HTTP_HOST']; ?> || 2020 </p>

</footer>