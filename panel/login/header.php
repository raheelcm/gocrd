<link rel="shortcut icon" type="image/png" href="images/favicon.png">
<?php
include "ini.php";
if(!isset($_SESSION['user_email'])){header('Location:login.php');exit;}
?>
<link rel="stylesheet" href="../../bootstrap.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="colorpick.css" rel="stylesheet">
<link href="customer_login_card.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script src="../../colorjs.js"></script>
<script src="../../jquery.min.js"></script>
<script src="../../bootstrap.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>
<header id="header" style="height:86px;padding-top: 16px;">
	<div class="logo" onclick="location.href='index.php'" style="margin-left: -48px;">
		<img src="images/Logo.png" style="width:100px; height:50px;" class="img-fluid" alt="logo">
		<h3 class="text-info">&nbsp;&nbsp;&nbsp;|&nbsp;Customer Page</h3>
	</div>
	<div class="mobile_home">&equiv;</div>
	<div class="head_txt">
		<h3><?php
			if(isset($_SESSION['user_email'])){
				echo '<b>Hi</b>! '.$_SESSION['user_email'];
			}else {echo 'Hi! Guest';}
			?>
		</h3>
		<h3>
			<a  class="text-danger" href="logout.php"><i class="fa fa-sign-out"></i> Logout</a>
		</h3>
	</div>
	

</header>


<script>

$(document).ready(function(){
	$('.mobile_home').on('click',function(){
		$('#header').toggleClass('add_height');
		
	})
})

</script>
<script>
$(document).ready(function(){
  $("form").submit(function(){
    $('#alert_display_full').css('display','block');
  });
});
</script>

<style>

</style>
<div id="alert_display_full">
	<div id="loader1"></div>
	<h3>Loading...</h3>

</div>