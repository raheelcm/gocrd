<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="colorpick.css" rel="stylesheet">
    
	<style>
		
	</style>
  </head>
  <body>
	

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
	<div class="container">
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
		  Launch demo modal
		</button>
		
		
	</div>
	
	
	
	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 95% !important;margin: 0 auto;">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Select Color For Theme</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<input type="text" id="color_val" class="form-control" value="" readonly>
		<div id="">
			<ul class="colorpicker">
			
				<!--<li class="no-color" data-color=""></li>-->
				<li class="gray-bg" data-color="#707070"></li>
				<li class="green-bg" data-color="#4caf50"></li>
				<li class="lightgreen-bg" data-color="#8cc152"></li>
				<li class="yellow-bg" data-color="#f7de30"></li>
				<li class="orange-bg" data-color="#fd9727"></li>
				<li class="deeporange-bg" data-color="#fc5830"></li>
				<li class="red-bg active" data-color="#e53935"></li>
				<li class="deeppurple-bg active" data-color="#673fb4"></li>
				<li class="blue-bg" data-color="#1976d2"></li>
				<li class="blue-bg" data-color="#1976d2"></li>
				<li class="blue-bg" data-color="#1976d2"></li>
				<li class="blue-bg" data-color="#1976d2"></li>
				<li class="lightblue-bg" data-color="#039be5"></li>
				<li class="lightblue-bg" data-color="#039be5"></li>
				<li class="lightblue-bg" data-color="#039be5"></li>
				<li class="lightblue-bg" data-color="#039be5"></li>
				<li class="lightblue-bg" data-color="#039be5"></li>
				<li class="lightblue-bg" data-color="#039be5"></li>
				<li class="lightblue-bg" data-color="#039be5"></li>
				<li class="lightblue-bg" data-color="#039be5"></li>
				<li class="lightblue-bg" data-color="#039be5"></li>
			</ul>
		</div>
		  </div>
		  <div class="modal-footer">
			<!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
			<button type="button" class="btn btn-primary">Save changes</button>
		  </div>
		</div>
	  </div>
	</div>
  </body> 
  <script>
	
	$( document ).ready(function() {
		$(".colorpicker li").click(function() {
			//alert($(this).attr("data-color"));
			$("#color_val").val($(this).attr(""));
			$("#color_val").val($(this).attr("data-color"));
			$(".colorpicker li").removeClass("active");
			$(this).addClass("active");
		});
	});
  </script>
</html>