<link rel="shortcut icon" type="image/png" href="images/favicon.png">
<?php
	require('ini.php');
	require('connect.php');
	if(!isset($_SESSION['f_user_email'])){header('LOCATION:login.php');}
	
?>
<link rel="stylesheet" href="../../bootstrap.min.css">
<link rel="stylesheet" href="../../colorstyle.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="colorpick.css" rel="stylesheet">
<title>Franchisee</title>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script src="../../jquery.min.js"></script>
<script src="../../bootstrap.min.js"></script>
<script src="../../colorjs.js"></script>
<script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>
<header id="header" style="height:86px;padding-top: 16px;">
	<div class="logo" onclick="location.href='index.php'">
		<img src="images/Logo.png" style="width:100px; height:50px;" class="img-fluid" alt="logo">
		<h3 class="text-info">&nbsp;&nbsp;&nbsp;|&nbsp;Partner Page</h3>
	</div>
	<div class="mobile_home">&equiv;</div>
	<div class="head_txt row">
		
		<h3>
			<a class="text-info" href="index.php"><i class="fa fa-home"></i> Home</a>
		</h3>
		
		<h3><?php
			if(isset($_SESSION['f_user_email'])){
			echo '<a href="my_account.php"><i class="fa fa-gear"></i> Setting</a>';
			}
			?>
			<h3>
				<a class="text-danger" href="logout.php"><i class="fa fa-sign-out"></i>Logout</a>
			</h3>
		</h3>
		<h3 class="text-warning"><?php
			if(isset($_SESSION['f_user_email'])){
			echo '<b>Hi!</b>'.$_SESSION['f_user_email'];
			}else {echo 'Hi! Guest';}
			?>
		</h3>
		<h3><img src="<?php
			$queryq=mysqli_query($connect,"SELECT * FROM franchisee_login WHERE f_user_email='$_SESSION[f_user_email]'");
			$rowq=mysqli_fetch_array($queryq);
			if(!empty($rowq['f_user_image'])){echo 'data:image/*;base64,'.base64_encode($rowq['f_user_image']);}else {echo 'images/logo.png';} ?>">
		</h3>
		
		
	</div>
	

</header>
<script>

$(document).ready(function(){
	$('.mobile_home').on('click',function(){
		$('#header').toggleClass('add_height');
		
	})
})

</script>


<script>
$(document).ready(function(){
  $("form").submit(function(){
    $('#alert_display_full').css('display','block');
  });
});
</script>


<div id="alert_display_full">
	<div id="loader1"></div>
	<h3>Loading...</h3>

</div>