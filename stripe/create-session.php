<?php
error_reporting(E_ALL);

// Same as error_reporting(E_ALL);
ini_set("error_reporting", E_ALL);

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);
require 'vendor/autoload.php';
//\Stripe\Stripe::setApiKey('sk_test_mSOHfomD5CDglqvXR9d7ImNs');
\Stripe\Stripe::setApiKey('sk_test_51Gr7KTHhSViwBiXreOFupmVhx6hoP3WenktEeCvmsr4NFRkvQo61KOeQcZd3j2Vg1M7CPUXX2VaYKoqVGJOmUa5F008ZtFTXep');

header('Content-Type: application/json');

$YOUR_DOMAIN = 'https://www.gocrd.in/stripe/';


try {
  $checkout_session = \Stripe\Checkout\Session::create([
  'payment_method_types' => ['card'],
  'line_items' => [[
    'price_data' => [
      'currency' => 'inr',
      'unit_amount' => 30000,
      'product_data' => [
        'name' => 'GoCrd.in',
        'images' => ["https://www.gocrd.in/admin/images/Logo.png"],
      ],
    ],
    'quantity' => 1,
  ]],
  'mode' => 'payment',
  'success_url' => $YOUR_DOMAIN . '/success.php',
  'cancel_url' => $YOUR_DOMAIN . '/cancel.html',
]);
  echo json_encode(['id' => $checkout_session->id]);
}

//catch exception
catch(Exception $e) {
  echo 'Message: ' .$e->getMessage();
}
