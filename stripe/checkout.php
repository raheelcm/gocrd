<?php
	//echo 'jskdfjksd';
	include 'connect.php';
	session_start();
	if(isset($_REQUEST)){
		//print_r($_REQUEST);
		//[id] => Testing2user [userEmail] => del2@gmail.com [username] =
		//die();
		$_SESSION['id'] = $_REQUEST['id'];
		$_SESSION['userEmail'] = $_REQUEST['userEmail'];
		$_SESSION['username'] = $_REQUEST['username'];
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Checkout</title>
    <link rel="stylesheet" href="style.css">
	<link rel='stylesheet' href='../panel/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
	<link rel="fav-icon" href="https://www.gocrd.in/admin/images/favicon.png" type="image/png">
	<link rel="stylesheet" href="../panel/awesome.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
    <script src="https://js.stripe.com/v3/"></script>
	<style>
	
		.collapse-content .fa.fa-heart:hover {
		  color: #f44336 !important;
		}
		.collapse-content .fa.fa-share-alt:hover {
		  color: #0d47a1 !important;
		}
		.bs-example{
    	margin: 20px;
		
		}
		.clip_path1 {
			clip-path: polygon(100% 0, 40% 100%, 0 100%, 0 58%);
			-webkit-clip-path: polygon(100% 0, 40% 100%, 0 100%, 0 58%);
			position: fixed;
			width: -webkit-fill-available;
			height: -webkit-fill-available;
		   background: repeating-linear-gradient(45deg, #3F51B5, transparent 23%), repeating-linear-gradient(-74deg, #9C27B0, transparent 19%), repeating-linear-gradient(5deg, #E91E63, transparent 31%);
			top:0;
			left:0;
		}
		body{
			background-color: #004085;
		}
	</style>
  </head>
  
  <body>
 
    <div style="width: 50%;">
			<!-- Card -->
		<div class="card promoting-card">

		  <!-- Card content -->
		  <div class="card-body d-flex flex-row">

			<!-- Avatar -->
			<img src="images/favicon.png" class="rounded-circle mr-3" style="width: 100px;height: 100px;" alt="avatar">

			<!-- Content -->
			<div>
			  <!-- Title -->
			  <h4 class="display-4 mt-4" style="font-size:40px">Confirm Your Details</h4>
			  <p>Activate Your Business Card (<?php echo $_REQUEST['id'];?>) For 1 Year</p>
			  <!-- Subtitle -->
			</div>

		  </div>
		  <!-- Card content -->
		  <div class="card-body">
			<div class="bs-example">
				<table class="table">
       
					<tbody>
						<tr>
							<td><b>Card ID</b></td>
						   
							<td><?php echo $_REQUEST['id'];?></td>
						</tr>
						<tr>
							<td><b>Name</b></td>
						   
							<td><?php echo $_REQUEST['username'];?></td>
						</tr>
						<tr>
							<td><b>Email</b></td>
							
							<td><?php echo $_REQUEST['userEmail'];?></td>
						</tr> 
						<tr>
							<td><b>Payment</b></td>
							
							<td><b><i class="fa fa-inr"></i>300</b></td>
						</tr> 
					</tbody>
				</table>
				</div>

		  </div>
			<button id="checkout-button">Checkout</button>
		</div>
			<!-- Card -->
			
  </body>
  <script type="text/javascript">
    // Create an instance of the Stripe object with your publishable API key
    //Raheel's test 
	//var stripe = Stripe("pk_test_lfuZ45UWiDbVVcChEqgJX7wu");
    //Clients live var stripe = Stripe("pk_live_RJUDDYkPCx0s6K20DoOhd4AM00uSIsKFvL");
    var stripe = Stripe("pk_test_51Gr7KTHhSViwBiXroZJubVWuovY8HltIm7irpD8lH9f0wQRg3i383oDROUJa4mraihwgpzdsAStwNcjXVz7yKXfh00HQUlgEaB");
    var checkoutButton = document.getElementById("checkout-button");

    checkoutButton.addEventListener("click", function () {
      fetch("https://www.gocrd.in/stripe/create-session.php?", {
        method: "POST",
      })
        .then(function (response) {
          return response.json();
        })
        .then(function (session) {
          return stripe.redirectToCheckout({ sessionId: session.id });
        })
        .then(function (result) {
          // If redirectToCheckout fails due to a browser or network
          // error, you should display the localized error message to your
          // customer using error.message.
          if (result.error) {
            alert(result.error.message);
          }
        })
        .catch(function (error) {
          console.error("Error:", error);
        });
    });
  </script>
</html>