<link rel="shortcut icon" type="image/png" href="images/favicon.png">
<?php
	if(!isset($_SESSION['admin_email'])){header('LOCATION:login.php');exit;}
	//print_r($_SESSION);
?>
	<link rel="stylesheet" href="../bootstrap.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="colorpick.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<script src="../../colorjs.js"></script>
	  <script src="../jquery.min.js"></script>
	  <script src="../bootstrap.min.js"></script>
	<script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>
	 <meta name="keywords" content="DIGI card, Digital Visiting Card">
 
	 <meta name="description" content="Best digital visiting card online with great design">
	  <!-- Required meta tags -->
	  <meta charset="utf-8" />
	  <link rel='stylesheet' href='../panel/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
	  
	  <link rel="stylesheet" href="../panel/awesome.min.css">
	 <meta      name='viewport'      content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
	<link rel="fav-icon" href="images/logo.png" type="image/png">
	<link rel="stylesheet" href="css.css" >
	<link rel="stylesheet" href="mobile_css.css" >
	<script src="master_js.js"></script>
	
	
	
<header id="header" style="height:86px;padding-top: 16px;">
	<div class="logo" onclick="location.href='index.php'">
		<img src="images/Logo.png" style="width:100px; height:50px;" class="" alt="logo">
		<h3 class="text-info">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;Admin</h3>
	</div>
	<div class="mobile_home">&equiv;</div>
	<div class="head_txt">
		<h3>
			<a class="text-info" href="index.php"><i class="fa fa-home"></i> Home</a>
		</h3>
		
		<h3>
			<?php
			if(isset($_SESSION['admin_email'])){
				echo '<a class="text-info" href="my_account.php"><i class="fa fa-gear"></i> Setting</a>';
			}
			?>
		</h3>
		<h3 class="text-warning"><?php
			if(isset($_SESSION['admin_email'])){
				echo '<b>Hi!</b> '.$_SESSION['admin_email'];
			}else {echo 'Hi Guest';}
			?>
		</h3>
		<h3>
			<a class="text-danger" href="logout.php"><i class="fa fa-sign-out"></i>Logout</a>
		</h3>
		
	</div>
	

</header>

<script>

$(document).ready(function(){
	$('.mobile_home').on('click',function(){
		$('#header').toggleClass('add_height');
		
	})
})

</script>

<script>
$(document).ready(function(){
  $("form").submit(function(){
    $('#alert_display_full').css('display','block');
  });
});
</script>


<div id="alert_display_full">
	<div id="loader1"></div>
	<h3>Loading...</h3>

</div>