<?php
	require('connect.php');
	if(!isset($_SESSION['admin_email']))
	{
	    header('LOCATION:login.php');
	    exit;
	}
	else
	{
	    
        if(isset($_GET['table']) && $_GET['table'] != "")
        {
            $table = $_GET['table'];
        	if(isset($table) && $table != "")
        	{
        	    $columnHeader = '';
        		$setData = '';
        		$tableFields = mysqli_query($connect, "SHOW COLUMNS FROM ".$table);
        		while($fieldNames = mysqli_fetch_array($tableFields))
        		{
        			$columnHeader .= $fieldNames['Field']."\t";
        		}
        		$result = mysqli_query($connect, "SELECT * FROM ".$table);		
        		while($rec = mysqli_fetch_row($result))
        		{ 
        			$rowData = ''; 
        			foreach ($rec as $value)
        			{ 
        				$value = '"'.$value.'"'."\t"; 
        				$rowData .= $value;
        			} 
        			$setData .= trim($rowData) . "\n"; 
        		}
        		
        		header('Content-Type: application/xls'); 
        		header("Content-Disposition: attachment; filename=".$table.".xls");
        		//header("Expires: 0"); 
        		echo $columnHeader . "\n" . $setData . "\n";
        	}
        }
	}
	
?>