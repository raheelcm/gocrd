<!DOCTYPE html> 
 <?php

require('connect.php');
require('header.php');

?>
<div class="filter_bar">
<a href="index.php"><h3 class="back_btn"><i class="fa fa-arrow-circle-left"></i> back </h3></a>
<h1 class="text-primary">Franchisee's Card Manager</h1>
	<h3>Filter </h3>
		<form action="">
			<select name="filter_option">
				<option value="">-Select-</option>
				<option>Payment Done</option>
				<option>Payment Not Done</option>
				<option>Trail Cards</option>
				
				
			</select>
			<input type="submit" name="filter">
		</form>
		
	<h3> Search</h3>
	<form action="">
			<input type="search" name="search_item" placeholder="Search id/Company/Franchisee/User">
			<input type="submit" name="search" value="Search">
		</form>
		
<?php

if(isset($_GET['filter'])){
				  
				  if($_GET['filter_option']=='Payment Done'){$filter="Success";}
				  else if($_GET['filter_option']=='Payment Not Done'){$filter="Created";}
				  else if($_GET['filter_option']=='Trail Cards'){$filter="Created";}
					else {$filter="All";}
					
			  }else {$filter="All";}
			  
			 
?>		
		
		
</div>

<!--------Top filter -------------------------->
<div class="container2">
	<div class="card_row">
	
		<p>Sr.No</p>
		<p>User ID</p>
		<p>Franchisee ID</p>
		<p>Record ID</p>
		<p>Card ID</p>
		<p>Company Name</p>
		<p>Payment Status</p>
		<p>Card Status</p>
		<p>Creation Date</p>
		<p>Exp. Date</p>
		<p>Payment Data</p>
		<p>Payment Amount</p>
		<p>Share</p>
		<p>Action</p>
		<p>Record Action</p>
		
		
	</div></br>
	
	<?php
	
	

	if(isset($_GET['page_no'])){
				
			}else {$_GET['page_no']='1';}

			
			 
			 $limit=30;
			 
			  $start_from=($_GET['page_no']-1)*$limit;
			  
// var case with filter
	$query=mysqli_query($connect,'SELECT *  FROM digi_card 
	WHERE
	CASE
    WHEN "'.$filter.'"="All" THEN   d_payment_status LIKE "%"  ELSE  d_payment_status="'.$filter.'"  
	END
	 AND f_user_email!="" 
	ORDER BY id DESC LIMIT '.$start_from.','.$limit.'');
// var case ends
		$count = 1;
		if(mysqli_num_rows($query)>>0){
			while($row=mysqli_fetch_array($query)){
				
				echo '<li class="card_row2">';
				echo '<p><b>'.$count.'</b></p>';
				echo '<p style="word-wrap: break-word;">'.$row['user_email'].'</p>';
				echo '<p style="word-wrap: break-word;">'.$row['f_user_email'].'</p>';
				echo '<p><a href="https://'.$_SERVER['HTTP_HOST'].'/'.$row['card_id'].'" target="_blank">'.$row['id'].'</p>';
				echo '<p style="word-wrap: break-word;">'.$row['card_id'].'</p>';
				echo '<p style="word-wrap: break-word;">'.$row['d_comp_name'].'<br> <i class="fa fa-external-link bg-info"></i></p></a>';
				echo '<p>'.$row['d_payment_status'].'</p>';
				echo '<p  >';
				if($row['d_payment_status']=='Created'){echo 'Trial Active';}else if($row['d_payment_status']=='Success'){echo 'Active';}else if($row['d_payment_status']=='Failed'){echo 'Inactive';}
				echo '</p>';

				echo '<p>'.$row['uploaded_date'].'</p>';
				//setting expiry date
					$date=date_create($row['uploaded_date']);
					date_modify($date,"+10 days");
					
					//date("Y-m-d h:i:sa", $d);
				echo '<p>'.date_format($date,"Y-m-d h:i:sa").'</p>';
				//expiry date END
				echo '<p>'.$row['d_payment_date'].'</p>';
				echo '<p>'.$row['d_payment_amount'].'</p>';
				echo '<p><a href="https://api.whatsapp.com/send?text=https://'.$_SERVER['HTTP_HOST'].'/'.$row['card_id'].'" target="_blank"><i class="bg success fa fa-whatsapp"></i></a><br><br><a href="https://www.facebook.com/sharer/sharer.php?u=https://'.$_SERVER['HTTP_HOST'].'/'.$row['card_id'].'" target="_blank"><i class="bg-primary fa fa-facebook"></i></a></p>';

				// activate card 
				echo '<p id="active_btn" class="idact'.$row['id'].'" onclick="activateUser('.$row['id'].')"><span class=" '.$row['d_card_status'].'">';
				echo $row['d_card_status'];
				echo '</span></p>';

				echo '<p><a href="select_theme.php?card_number='.$row['id'].'&user_email='.$row['user_email'].'"><i class="fa fa-edit bg-warning"></i></a><br><br>';
				echo '<a onclick="de_f_card('.$row['id'].')");" href="javascript:void(0)"><i class="fa fa-trash text-danger"></i></a></p>';
				echo '</li><hr>';
				$count++;
			}
		}else {
			echo '<div class="alert info">No Data Available...</div>';
		}
	?>
	

</div>

<!-------------------Pagination-------------------->
		<div class="pagination">
			<?php 



				

				$query2=mysqli_query($connect,'SELECT * FROM digi_card ORDER BY id DESC ');
			
			 $pages=ceil(mysqli_num_rows($query2)/30);

			for($i=1;$i<=$pages;$i++){
				if($_GET['page_no']==$i){
					echo '<a href="?page_no='.$i.'&filter_option='.$filter.'&filter=Submit"><div class="page_btn active">'. $i.'</div></a>';
				}else {
					echo '<a href="?page_no='.$i.'"><div class="page_btn">'. $i.'</div></a>';
				}
				
			}


			?>
	</div>

<!-------------------Pagination-------------------->

	<script>
							
		// if approved
		function activateUser(id){
				
				$('.idact'+id).css('color','blue').html('Wait...');
			
				$.ajax({
					url:'js_request.php',
					method:'POST',
					data:{card_id:id,activate_user:'YES'},
					dataType:'text',
					success:function(data){
						$('.idact'+id).html(data);
					}
					
				});
				
		}
		function de_f_card(crd_id){
			//alert(crd_id);
			if(confirm("Are You Sure Deleting This Card?")){
				$.post("js_request.php", {card_id:crd_id,del_f_card:'YES'}, function(result){
					//alert(result);
					location.reload();
				});
			}
		}
	</script>
							
							

<footer class="">

<p>Copyright 2020 || <?php echo $_SERVER['HTTP_HOST']; ?></p>

</footer>