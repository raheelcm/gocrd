 <?php

require('connect.php');
require('header.php');

?>

<?php
$query=mysqli_query($connect,'SELECT * FROM digi_card WHERE id="'.$_SESSION['card_id_inprocess'].'" ');

if(mysqli_num_rows($query)==0){
	echo '<meta http-equiv="refresh" content="0;URL=index.php">';
}else {
	$row=mysqli_fetch_array($query);
}

/*   for nature of business */
$query3=mysqli_query($connect,'SELECT business_nature FROM business_nature where business_nature not in (SELECT user_nature_business FROM digi_card4 where digi_card_id="'.$_SESSION['card_id_inprocess'].'")');
$query4=mysqli_query($connect,'SELECT business_nature FROM business_nature where business_nature IN (SELECT user_nature_business FROM digi_card4 where digi_card_id="'.$_SESSION['card_id_inprocess'].'")');

$query2=mysqli_query($connect,'SELECT user_nature_business FROM digi_card4 where digi_card_id="'.$_SESSION['card_id_inprocess'].'" and  user_nature_business not in (SELECT business_nature FROM business_nature) and user_nature_business!=""');
$queryNew=mysqli_query($connect,'SELECT user_nature_business FROM digi_card4 where digi_card_id="'.$_SESSION['card_id_inprocess'].'" and  user_nature_business!=""');

/*   for nature of business */

/*   for Specialities*/
$special1=mysqli_query($connect,'SELECT specialities FROM specialities where specialities not in (SELECT user_specialities FROM digi_card5 where digi_card_id="'.$_SESSION['card_id_inprocess'].'")');
$special2=mysqli_query($connect,'SELECT specialities FROM specialities where specialities IN (SELECT user_specialities FROM digi_card5 where digi_card_id="'.$_SESSION['card_id_inprocess'].'")');
$special3=mysqli_query($connect,'SELECT user_specialities FROM digi_card5 where digi_card_id="'.$_SESSION['card_id_inprocess'].'" and  user_specialities not in (SELECT specialities FROM specialities) and user_specialities!=""');
$specialAll=mysqli_query($connect,'SELECT user_specialities FROM digi_card5 where digi_card_id="'.$_SESSION['card_id_inprocess'].'" and  user_specialities!=""');

/*   for Specialities */

?>

<div class="main3">
<div class="navigator_up">
		<a href="select_theme.php"><div class="nav_cont  " ><i class="fa fa-map"></i> Select Theme</div></a>
		<a href="create_card2.php"><div class="nav_cont "><i class="fa fa-bank"></i> Company Details</div></a>
		<a href="create_card3.php"><div class="nav_cont active"><i class="fa fa-facebook"></i> Social Links</div></a>
		<a href="create_card4.php"><div class="nav_cont"><i class="fa fa-rupee"></i> Payment Options</div></a>
		<a href="create_card5.php"><div class="nav_cont "><i class="fa fa-ticket"></i> Products & Services</div></a>
		<a href="create_card7.php"><div class="nav_cont"><i class="fa fa-archive"></i> Order Page</div></a>
		<a href="create_card6.php"><div class="nav_cont"><i class="fa fa-image"></i> Image Gallery</div></a>
		<a href="create_card8.php"><div class="nav_cont "><i class="fa fa-cog"></i> Settings</div></a>
		<a href="preview_page.php"><div class="nav_cont"><i class="fa fa-laptop"></i> Preview Card</div></a>
	
	</div>
	
	<div class="btn_holder">
		<a href="create_card2.php"><div class="back_btn"><i class="fa fa-chevron-circle-left"></i> Back</div></a>
		<a href="create_card4.php"><div class="skip_btn">Skip <i class="fa fa-chevron-circle-right"></i></div></a>
	</div>
	<h1>Nature of business  & Specialities </h1>
	
	<form action="" method="POST" enctype="multipart/form-data">
	
         <div class="input_box" style="min-height: 60px;">
		<p>Nature of business * </p>
		<button  style="float:right;margin-right:10px;margin-top:8px;" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"> + Add </button><br><br>
		<div id="natureofbusiness">
				<?php if(mysqli_num_rows($queryNew)>>0){
			while($all=mysqli_fetch_array($queryNew)){ ?>
		<button type="button" class="btn btn-secondary btn-xs extra-css"  ><?php echo $all['user_nature_business'];?>&nbsp;&nbsp;<i class="fa fa-times" onclick="deleteBusinessNature('<?php echo $all['user_nature_business'];?>')"></i>
		<?php }} ?>
		</div>
		</div>
			<div class="input_box" style="min-height: 60px;">
		<p>Specialities * </p>
		<button  style="float:right;margin-right:10px;margin-top:8px;" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal2"> + Add </button><br><br>
		<div id="all_specialities">
				<?php if(mysqli_num_rows($specialAll)>>0){
			while($all=mysqli_fetch_array($specialAll)){ ?>
		<button type="button" class="btn btn-secondary btn-xs extra-css"  ><?php echo $all['user_specialities'];?>&nbsp;&nbsp;<i class="fa fa-times" onclick="deleteSpecialities('<?php echo $all['user_specialities'];?>')"></i>
		<?php }} ?>
		</div>
		</div>
<!-------------------form ----------------------->	
		<h3>Social Media Links</h3>
		<div class="input_box"><p>Facebook Link(Optional)</p><input type="text" name="d_fb" maxlength="200" placeholder="facebook Link" value="<?php if(!empty($row['d_fb'])){echo $row['d_fb'];}?>" ></div>
		
		<div class="input_box"><p>Twitter Link(Optional)</p><input type="text" name="d_twitter" maxlength="200" placeholder="Twitter Link " value="<?php if(!empty($row['d_twitter'])){echo $row['d_twitter'];}?>"></div>
		
		<div class="input_box"><p>Instagram Link(Optional) </p><input type="text" name="d_instagram" maxlength="200" placeholder="Instagram Link" value="<?php if(!empty($row['d_instagram'])){echo $row['d_instagram'];}?>" ></div>
		
		<div class="input_box"><p>LinkedIn Link(Optional)</p><input type="text" name="d_linkedin" maxlength="200" placeholder="Linked in Link" value="<?php if(!empty($row['d_linkedin'])){echo $row['d_linkedin'];}?>" ></div>
		
		<div class="input_box"><p>Youtube Link(Optional)</p><input type="text" name="d_youtube" maxlength="200" placeholder="Youtube Page Link" value="<?php if(!empty($row['d_youtube'])){echo $row['d_youtube'];}?>" ></div>
		
		<div class="input_box"><p>Pinterest Link(Optional)</p><input type="text" name="d_pinterest" maxlength="200" placeholder="Pinterest Link"  value="<?php if(!empty($row['d_pinterest'])){echo $row['d_pinterest'];}?>" ></div>
		
		<h3>Youtube Video Links</h3>
		<div class="input_box"><p>Youtube Video Link (Optional)</p><input type="text" name="d_youtube1" maxlength="200" placeholder="1st Youtube Video Link "  value="<?php if(!empty($row['d_youtube1'])){echo $row['d_youtube1'];}?>" ></div>
		<div class="input_box"><p>Youtube Video Link 2(Optional)</p><input type="text" name="d_youtube2" maxlength="200" placeholder="2nd Youtube Video Link"  value="<?php if(!empty($row['d_youtube2'])){echo $row['d_youtube2'];}?>" ></div>
		<div class="input_box"><p>Youtube Video Link 3(Optional)</p><input type="text" name="d_youtube3" maxlength="200" placeholder="3rd Youtube Video Link"  value="<?php if(!empty($row['d_youtube3'])){echo $row['d_youtube3'];}?>" ></div>
		<div class="input_box"><p>Youtube Video Link 4(Optional)</p><input type="text" name="d_youtube4" maxlength="200" placeholder="4th Youtube Video Link"  value="<?php if(!empty($row['d_youtube4'])){echo $row['d_youtube4'];}?>" ></div>
		<div class="input_box"><p>Youtube Video Link 5(Optional)</p><input type="text" name="d_youtube5" maxlength="200" placeholder="5th Youtube Video Link"  value="<?php if(!empty($row['d_youtube5'])){echo $row['d_youtube5'];}?>" ></div>
		
		
		
		<input type="submit" class="" name="process3" value="Next 4" id="block_loader">
	
<!-------------------form ending----------------------->
	</form>
	
			  <!-- Modal Nature Of Business-->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Business Nature</h4>
        </div>
		<form id="nature_form" class="nature_form"  action="" method="POST" enctype="multipart/form-data" >
        <div class="modal-body" style=" height: 430px;overflow-y: auto;">
		<h4>Default Nature of business list</h4>
		<?php if(mysqli_num_rows($query3)>>0){
			while($nature=mysqli_fetch_array($query3)){ ?>
          <input type="checkbox" name="business_nature_new[]" value="<?php if(!empty($nature['business_nature'])){echo $nature['business_nature'];}?>" >&nbsp;&nbsp;<?php if(!empty($nature['business_nature'])){echo $nature['business_nature'];}?><br><br>
		<?php }} ?>
		<?php if(mysqli_num_rows($query4)>>0){
			while($natureold=mysqli_fetch_array($query4)){ ?>
			<input type="hidden" name="existing_nature[]" value="<?php if(!empty($natureold['business_nature'])){echo $natureold['business_nature'];}?>">
          <input type="checkbox" name="business_nature_old[]" checked value="<?php if(!empty($natureold['business_nature'])){echo $natureold['business_nature'];}?>" >&nbsp;&nbsp;<?php if(!empty($natureold['business_nature'])){echo $natureold['business_nature'];}?><br><br>
		<?php }} ?>
		<hr>
		<h4>Manual Nature of business</h4>
		<div class="input_box_new"><input type="text" style="height:50px !important;" name="manual_business_nature" id="manual_business_nature"  placeholder="Nature of business" class="form-control" value="" ></div>
       <br>
	   <button type="button" class="btn btn-primary" onclick="addBusinessNatureManual()">Add</button>
	   <br>
	   <br>
		  <div id="natures">
			<?php if(mysqli_num_rows($query2)>>0){
			while($mn=mysqli_fetch_array($query2)){ ?>
			<span><?php echo $mn['user_nature_business']; ?><button type="button" class="btn btn-danger" onclick="deleteBusinessNature('<?php echo $mn['user_nature_business']; ?>')" style="float:right;"><i class="fa fa-trash"></i></button></span><hr>
			<?php }} ?>
		   </div>
		</div>
        <div class="modal-footer">
          <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" id="AddNature" name="AddNature" class="btn btn-success" >Done</button>
        </div>
		</form>
      </div>
    </div>
  </div>
  
  	  <!-- Modal Specialities-->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Speciality</h4>
        </div>
		<form id="nature_form" class="nature_form"  action="" method="POST" enctype="multipart/form-data" >
        <div class="modal-body" style=" height: 430px;overflow-y: auto;">
		<h4>Default Speciality list</h4>
		<?php if(mysqli_num_rows($special1)>>0){
			while($row1=mysqli_fetch_array($special1)){ ?>
          <input type="checkbox" name="specialities_new[]" value="<?php if(!empty($row1['specialities'])){echo $row1['specialities'];}?>" >&nbsp;&nbsp;<?php if(!empty($row1['specialities'])){echo $row1['specialities'];}?><br><br>
		<?php }} ?>
		<?php if(mysqli_num_rows($special2)>>0){
			while($row2=mysqli_fetch_array($special2)){ ?>
			<input type="hidden" name="existing_speciality[]" value="<?php if(!empty($row2['specialities'])){echo $row2['specialities'];}?>">
          <input type="checkbox" name="specialities_old[]" checked value="<?php if(!empty($row2['specialities'])){echo $row2['specialities'];}?>" >&nbsp;&nbsp;<?php if(!empty($row2['specialities'])){echo $row2['specialities'];}?><br><br>
		<?php }} ?>
		<hr>
		<h4>Manual Speciality</h4>
		<div class="input_box_new"><input type="text" style="height:50px !important;" name="manual_speciality" id="manual_speciality"  placeholder="Speciality" class="form-control" value="" ></div>
       <br>
	   <button type="button" class="btn btn-primary" onclick="addSpecialityManual()">Add</button>
	   <br>
	   <br>
		  <div id="specialtiy">
			<?php if(mysqli_num_rows($special3)>>0){
			while($row3=mysqli_fetch_array($special3)){ ?>
			<span><?php echo $row3['user_specialities']; ?><button type="button" class="btn btn-danger" onclick="deleteSpecialities('<?php echo $row3['user_specialities']; ?>')" style="float:right;"><i class="fa fa-trash"></i></button></span><hr>
			<?php }} ?>
		   </div>
		</div>
        <div class="modal-footer">
          <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" id="AddSpeciality" name="AddSpeciality" class="btn btn-success" >Done</button>
        </div>
		</form>
      </div>
    </div>
  </div>
  
  
  <script>
       function addBusinessNatureManual()
 {
   var processadd=true
   var nature = $('#manual_business_nature').val();
    if(nature!='')
    {
        $.ajax(
            {
                type:"POST", 
                url:"create_card3.php",
                data:{'nature':nature,'processadd':processadd},
                success:function(data){
                   $('#manual_business_nature').val('');
				   $("#natures").load("create_card3.php #natures"); 
                }
            });
    }
 }
 function deleteBusinessNature(nature)
 {
    var deleteenature=true;
	if(nature!='')
    {
        $.ajax(
            {
                type:"POST", 
                url:"create_card3.php",
                data:{'nature':nature,'deleteenature':deleteenature},
                success:function(data){
                  $("#natures").load("create_card3.php #natures"); 
                  $("#natureofbusiness").load("create_card3.php #natureofbusiness"); 
                }
            });
    }
 }
 
  function addSpecialityManual()
 {
   var specialtiyAdd=true
   var specialtiy = $('#manual_speciality').val();
    if(specialtiy!='')
    {
        $.ajax(
            {
                type:"POST", 
                url:"create_card3.php",
                data:{'specialtiy':specialtiy,'specialtiyAdd':specialtiyAdd},
                success:function(data){
                   $('#manual_speciality').val('');
				   $("#specialtiy").load("create_card3.php #specialtiy"); 
                }
            });
    }
 }
 function deleteSpecialities(specialtiy)
 {
    var deleteSpeciality=true;
	if(specialtiy!='')
    {
        $.ajax(
            {
                type:"POST", 
                url:"create_card3.php",
                data:{'specialtiy':specialtiy,'deleteSpeciality':deleteSpeciality},
                success:function(data){
                  $("#specialtiy").load("create_card3.php #specialtiy"); 
                  $("#all_specialities").load("create_card3.php #all_specialities"); 
                }
            });
    }
 } 
  </script>
  
  <?php 
if(isset($_POST['processadd']))
{ 
	$query=mysqli_query($connect,'SELECT * FROM digi_card WHERE id="'.$_SESSION['card_id_inprocess'].'"');
		if(mysqli_num_rows($query)==1)
		{  
		   $row=mysqli_fetch_array($query);
		    $email=$row['user_email'];
			$insert=mysqli_query($connect,'INSERT INTO digi_card4 (digi_card_id,user_email,user_nature_business) VALUES ("'.$_SESSION['card_id_inprocess'].'","'.$email.'","'.$_POST['nature'].'")') or die('mysql error');
			if($insert)
			{
				echo "success";	
			}
			else
			{
				echo "error";	
			}
		}
}

if(isset($_POST['deleteenature'])){
	$query=mysqli_query($connect,'SELECT * FROM digi_card WHERE id="'.$_SESSION['card_id_inprocess'].'"');
		if(mysqli_num_rows($query)==1)
		{
			$delete=mysqli_query($connect,'DELETE  from digi_card4  WHERE digi_card_id="'.$_SESSION['card_id_inprocess'].'" and user_nature_business="'.$_POST['nature'].'"') or die('mysql error');
			if($delete)
			{
				echo "success";	
			}
			else
			{
				echo "error";	
			}
		}
}

if(isset($_POST['AddNature']))
{ 
	$query=mysqli_query($connect,'SELECT * FROM digi_card WHERE id="'.$_SESSION['card_id_inprocess'].'"');
		if(mysqli_num_rows($query)==1)
		{  
		   $row=mysqli_fetch_array($query);
		    $email=$row['user_email'];
			$nature=$_POST['business_nature_new'];
			
			for($i=0;$i<count($nature);$i++)
			{  
			   $insert=mysqli_query($connect,'INSERT INTO digi_card4 (digi_card_id,user_email,user_nature_business) VALUES ("'.$_SESSION['card_id_inprocess'].'","'.$email.'","'.$nature[$i].'")');
		    }
			$nature_old=$_POST['business_nature_old'];
			$existing_nature=$_POST['existing_nature'];
			for($j=0;$j<count($existing_nature);$j++)
			{  
				if(empty($nature_old[$j]))
				{
			      $delete=mysqli_query($connect,'DELETE  from digi_card4  WHERE digi_card_id="'.$_SESSION['card_id_inprocess'].'" and user_nature_business="'.$existing_nature[$j].'"') ;
				}
		    }
			
			echo '<a href="create_card3.php"><div class="alert info"> Wait...</div></a>';
		    echo '<meta http-equiv="refresh" content="0;URL=create_card3.php">';
	  }
			
}

?>

<!--     Specialties    -->

<?php 
if(isset($_POST['specialtiyAdd']))
{ 
	$query=mysqli_query($connect,'SELECT * FROM digi_card WHERE id="'.$_SESSION['card_id_inprocess'].'"');
		if(mysqli_num_rows($query)==1)
		{  
		   $row=mysqli_fetch_array($query);
		    $email=$row['user_email'];
			$insert=mysqli_query($connect,'INSERT INTO digi_card5 (digi_card_id,user_email,user_specialities) VALUES ("'.$_SESSION['card_id_inprocess'].'","'.$email.'","'.$_POST['specialtiy'].'")') or die('mysql error');
			if($insert)
			{
				echo "success";	
			}
			else
			{
				echo "error";	
			}
		}
}

if(isset($_POST['deleteSpeciality'])){
	$query=mysqli_query($connect,'SELECT * FROM digi_card WHERE id="'.$_SESSION['card_id_inprocess'].'"');
		if(mysqli_num_rows($query)==1)
		{
			$delete=mysqli_query($connect,'DELETE  from digi_card5  WHERE digi_card_id="'.$_SESSION['card_id_inprocess'].'" and user_specialities="'.$_POST['specialtiy'].'"') or die('mysql error');
			if($delete)
			{
				echo "success";	
			}
			else
			{
				echo "error";	
			}
	  }
}

if(isset($_POST['AddSpeciality']))
{ 
	$query=mysqli_query($connect,'SELECT * FROM digi_card WHERE id="'.$_SESSION['card_id_inprocess'].'"');
		if(mysqli_num_rows($query)==1)
		{  
		   $row=mysqli_fetch_array($query);
		    $email=$row['user_email'];
			$specialtiy=$_POST['specialities_new'];
			
			for($i=0;$i<count($specialtiy);$i++)
			{  
			   $insert=mysqli_query($connect,'INSERT INTO digi_card5 (digi_card_id,user_email,user_specialities) VALUES ("'.$_SESSION['card_id_inprocess'].'","'.$email.'","'.$specialtiy[$i].'")');
		    }
			$specialtiy_old=$_POST['specialities_old'];
			$existing_speciality=$_POST['existing_speciality'];
			for($j=0;$j<count($existing_speciality);$j++)
			{  
				if(empty($specialtiy_old[$j]))
				{
			      $delete=mysqli_query($connect,'DELETE  from digi_card5  WHERE digi_card_id="'.$_SESSION['card_id_inprocess'].'" and user_specialities="'.$existing_speciality[$j].'"') ;
				}
		    }
			echo '<a href="create_card3.php"><div class="alert info"> Wait...</div></a>';
		    echo '<meta http-equiv="refresh" content="0;URL=create_card3.php">';
	  }
			
}

?>


<!--     Specialties    -->	
	
	<?php
	if(isset($_POST['process3'])){
		
		$query=mysqli_query($connect,'SELECT * FROM digi_card WHERE id="'.$_SESSION['card_id_inprocess'].'"');
		if(mysqli_num_rows($query)==1){
			
		// enter details in database
				
			$update=mysqli_query($connect,'UPDATE digi_card SET 
			
			d_fb="'.$_POST['d_fb'].'",
			d_twitter="'.$_POST['d_twitter'].'",
			d_instagram="'.$_POST['d_instagram'].'",
			d_linkedin="'.$_POST['d_linkedin'].'",
			d_youtube="'.$_POST['d_youtube'].'",
			d_pinterest="'.$_POST['d_pinterest'].'",
			d_youtube1="'.$_POST['d_youtube1'].'",
			d_youtube2="'.$_POST['d_youtube2'].'",
			d_youtube3="'.$_POST['d_youtube3'].'",
			d_youtube4="'.$_POST['d_youtube4'].'",
			d_youtube5="'.$_POST['d_youtube5'].'"
			WHERE id="'.$_SESSION['card_id_inprocess'].'"');
			
		// enter details in database ending
		
		if($update){
			echo '<a href="create_card4.php"><div class="alert info">Details Updated Wait...</div></a>';
			echo '<meta http-equiv="refresh" content="0;URL=create_card4.php">';
			echo '<style>  form {display:none;} </style>';
		}else {
			echo '<a href="create_card3.php"><div class="alert danger">Error! Try Again.</div></a>';
		}
			
		
		}else {
			
			echo '<a href="create_card.php"><div class="alert danger">Detail Not Available. Try Again Click here.</div></a>';
		}
		
	}
	?>

</div>


<footer class="">

<p> <?php echo $_SERVER['HTTP_HOST']; ?> || 2020 </p>

</footer>