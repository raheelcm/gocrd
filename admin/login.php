
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Admin</title>
	<style>
		.login,
		.image {
		  min-height: 100vh;
		}

		.bg-image {
		 /** background-image: url('https://res.cloudinary.com/mhmd/image/upload/v1555917661/art-colorful-contemporary-2047905_dxtao7.jpg'); **/
		  background-image: url('../admin/images/bg.jpg');
		  background-size: cover;
		  background-position: center center;
		}
	</style>
  </head>
<body>
<div class="container-fluid">
    <div class="row no-gutter">
        <!-- The image half -->
        <div class="col-md-6 d-none d-md-flex bg-image">
			
		<!--	<h3 class="display-4" style="font-size: 44px;color: white;text-align: center;padding: 180px 3px 4px 190px;"><img src="../images/Logo.png" width="200" height="65" alt="image"></h3> -->
		</div>


        <!-- The content half -->
        <div class="col-md-6 bg-light">
            <div class="login d-flex align-items-center py-5">
                <!-- Demo content-->
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 col-xl-7 mx-auto">
                            <h2 class="display-4" style="font-size:42px;">Gocrd Admin Login</h2></br>
                            <p class="text-muted mb-4 ">Enter your credentials to access the Admin Dashboard</p>
                            <form action="" method="post" autocomplete="off">
                                <div class="form-group mb-3">
                                    <input id="inputEmail" name="admin_email"type="text" placeholder="Username" required="" autofocus="" class="form-control rounded-pill border-0 shadow-sm px-4">
                                </div>
                                <div class="form-group mb-3">
                                    <input id="inputPassword" name="admin_password" type="password" placeholder="Password" required="" class="form-control rounded-pill border-0 shadow-sm px-4 text-primary">
                                </div>
                                <input name="login_user" type="submit" value="Login" class="btn btn-primary btn-block text-uppercase mb-2 rounded-pill shadow-sm">
                                
                            </form>
							<?php
	date_default_timezone_set("Asia/Kolkata");
	session_start();
	if($_SERVER['HTTP_HOST']=="localhost"){$connect=mysqli_connect("localhost","root","","gocrd") or die ('Database not available...');}else {$connect=mysqli_connect("localhost","u888486731_gocrd","Hello1212!@","u888486731_gocrd") or die ('Connection issue #567844 Error');}
	$date=date('Y-m-d H:i:s');
	//require('connect.php');
	if(isset($_POST['login_user'])){
		$query=mysqli_query($connect,'SELECT * FROM admin_login WHERE admin_email="'.$_POST['admin_email'].'" OR admin_contact="'.$_POST['admin_email'].'" ');
		if(mysqli_num_rows($query)>0){
			//login function 
			$row=mysqli_fetch_array($query);
			
			if($row['admin_password']==$_POST['admin_password'] ){
				
					$_SESSION['admin_email']=$row['admin_email'];
					$_SESSION['admin_name']=$row['admin_name'];
					$_SESSION['admin_contact']=$row['admin_contact'];
					echo '<div class="alert alert-success">Login Success, Redirecting...</div>';
					echo '<meta http-equiv="refresh" content="0;URL=index.php">';
			}else {
				echo '<div class="alert alert-danger">Password Wrong! Try Again.</div>';
			}
			
		}else {
			echo '<div class="alert alert-danger" id="register_en">User Does Not Exists. Create New Account</div>';
		}
		//echo "kaddooo";
		//die();
	}
	

?>
                        </div>
						
                    </div>
                </div><!-- End -->

            </div>
			
        </div><!-- End -->

    </div>
</div>


<!-- Material form subscription -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>